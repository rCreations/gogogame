package main

import "fmt"
import "bufio"
import "os"
import "time"
import "strings"
import "strconv"

var player map[string]string
var inventory []string

var debug bool
var version string = "0.1"

func init() {
	/* init */
	if (len(os.Args) > 1 && os.Args[1] == "debug") {
		debug = true
		fmt.Println("DEBUGMODE")
	}

	fmt.Printf("Loading"); Pause(1); Say("...", 1000)
	player = make(map[string]string)
}

func main() {
	/* start */
	fmt.Println(" _____     _____     _____")
	fmt.Println("|   __|___|   __|___|   __|___ _____ ___ ")
	fmt.Println("|  |  | . |  |  | . |  |  | .'|     | -_|")
	fmt.Println("|_____|___|_____|___|_____|__,|_|_|_|___|")
	fmt.Println("                          by whiteRabbit \n\n")

	for(player["name"] == "") {
		fmt.Printf("Type your name to start: ")
		player["name"] = Input()
	}

	Say("Hello " + player["name"] + ", your journey begins here...", 80)
	Pause(1)
	Say("Now all we need is a story...", 80)
	Pause(1)
	Say("Okay, let me generate one. Please wait.", 80)
	Pause(1)
	Say("...",  333)
	Pause(1)

	forest();

	for {

	}
}

/* scenes */

func forest() {
	Say("You wake up without any memory.", 80)
	Pause(1)
	Say("\"Where am I?!\"", 120)
	Say("You feel wet and cold, then you realize you woke up in a dense forest.", 80)
	Say("It looks like night is coming very, very soon.", 80)	
	for answer := ""; answer == ""; {
		Say("\nYou look around and see [1] a rock and [2] a narrow path through the forest.", 80)
		fmt.Println("What do you do?")
		Pause(1)
		fmt.Println("[1] Try to lift the rock.")
		Pause(1)
		fmt.Println("[2] Screw you rock, I'm going home!")
		Pause(1)
		fmt.Println("[3] You start to cry. (You will probably die beacuse it will be night soon. And, you know.. Monsters.)")
		Pause(1)
		fmt.Printf("Type the number of one of the options or type 'help' to show all commands: ")
		answer = Input()

		switch answer {
		case "1":
			if hasItem("Sharp stone")<0 {
				Say("You lift the rock, but it slips through your hands.", 80)
				Pause(1)
				Say("A sharp piece broke off, you pick it up.", 80)
				addItem("Sharp stone")
			} else {
				Say("Nah, it's heavy.", 50)
			}
			answer = ""
		case "2":
			Say("You start walking for a while", 50)
			caveentrance()
		case "3":
			nightforest()
		default:
			command(answer)
			answer = ""
		}
	}
}

func nightforest() {
	Say("You decided to cry.", 80)
	Pause(1)
	fmt.Printf("3 hours later"); Pause(1); Say("...", 1000)
	Say("It's getting really dark, you can barely see.", 80)
	Pause(1)
	Say("Maybe this wasn't such a good idea?", 60)
	Pause(1)
	Say("You hear something behind you in the tall grass, but it is too dark to see what it is.", 50)
	if hasItem("Sharp stone")>=0 {
		Say("You take the sharp stone from your backpack and hold it very tightly, as it may be your only chance of survival.", 80)
		Pause(1)
		for answer := ""; answer == ""; {
			Say("\nYou heard something, again.", 80)
			Pause(1)
			fmt.Println("[1] Throw the stone at the tall grass, hoping you hit the thing that is making the strange noises")
			Pause(1)
			fmt.Println("[2] Run at the tall grass and try to stab anything that moves!")
			Pause(1)
			fmt.Println("[3] Lose this beautiful stone?! No way!")
			Pause(1)
			fmt.Printf("Type the number of one of the options or type 'help' to show all commands: ")
			answer = Input()

			switch answer {
			case "1":
				Say("You throw the stone, it seems like you hit it.", 80)
				removeItem("Sharp stone")
				Say("Too scared to look, you just start running away from the tall grass...", 80)
				Pause(1)
				caveentrance()
			case "2":
				Say("You run through the tall grass.", 60)
				Say("A wild Pickachu appeared!", 50)
				Say("\"WTF?!\"", 120)
				Pause(1)
				Say("Pickachu fled!", 50)
				Say("\"Well, that was weird\"", 80)
				Pause(1)
				fmt.Println("Rain starts falling.")
				Say("\"Maybe I should go seek some shelter, before more weird things start to happen.\"", 80)
				Say("You turn around and start walking...", 80)
				caveentrance()
			case "3":
				Say("You feel something stinging in your back.", 80)
				Say("Everything becomes blurry...", 80)
				Pause(2)
				youDie()
			default:
				command(answer)
				answer = ""
			}
		}
	} else {
		Say("You feel something stinging in your back.", 80)
		Say("Everything becomes blurry...", 80)
		Pause(2)
		youDie()
	}
}

func caveentrance() {
	for answer := ""; answer == ""; {
		Say("You see a cave entrance.", 80)
		fmt.Printf("Go inside? (y/n): ")
		answer = Input()

		switch answer {
		case "Y", "y":
			Say("cave", 80)
		case "N", "n":
			Say("nope", 80)
		default:
			command(answer)
			answer = ""
		}
	}
	Say("Thank you for playing this preprepre alpha version of this game! Restart and try some more posibilities?", 50)
	Input()
	youDie()
}


/* main 'game' functions */

/* inventory */

func hasItem(item string) (index int) {
	for i:=0;i<len(inventory);i++ {
		if inventory[i] == item {
			return i
		}
	}
	return -1
}

func addItem(item string) {
	inventory = append(inventory, item) 
	Say("\""+ item +"\" added to inventory!", 50)
}

func removeItem(item string) {
	i := hasItem(item)
	if i >= 0 {
		inventory = append(inventory[:i], inventory[i+1:]...)
		Say("\""+ item +"\" removed from inventory.", 50)
	}
}


/* misc */

func youDie() {
	inventory = inventory[:0]
	//hint()
	forest()
}

/* main 'engine' functions */

func Input() (line string) {
	read, err := bufio.NewReader(os.Stdin).ReadString('\n')
	if err != nil {
		fmt.Println("Error!")
	}
		read = strings.Replace(read, "\r\n", "", -1)
		read = strings.Replace(read, "\n", "", -1)
	return read 
}

func Say(text string, speed int) {
	if !debug {
		duration := time.Duration(speed)*time.Millisecond
		for i:=0; i<len(text); i++ {
			fmt.Print(text[i:i+1]);
			time.Sleep(duration)
		}
		fmt.Printf("\n")
	} else {
		fmt.Println(text);
	}
}

func Pause(duration int) {
	if !debug {
		time.Sleep(time.Duration(duration)*time.Second)
	}
}


/* commands */

func command(command string) {
	switch command {
	case "i", "inventory", "backpack":
		showItems()
	case "?", "h", "help":
		showHelp()
	case "s", "skills", "stats":
		showStats()
	default:
		Say("Invalid answer, please type a number or a valid command. Type 'help' to show all commands.", 50)
	}
}

func showItems() {
	Say("Inventory items ("+ strconv.Itoa(len(inventory)) +")" , 50)
	for i:=0;i<len(inventory);i++{
		Say("\t"+ inventory[i] +", ", 50)
	}
}

func showStats() {
	Say("Not yet implemented", 50)
	return
}

func showHelp() {
	fmt.Println(" ______________________________________________________________")
	fmt.Println("|                         GoGoGame Help                        |")
	fmt.Println("|                                                              |")
	fmt.Println("|  Commands can be entered anytime when asked for input        |")
	fmt.Println("|                                                              |")
	fmt.Println("|  Command list:                                               |")
	fmt.Println("|    ?, h, help:                                               |")
	fmt.Println("|        Will show this help screen                            |")
	fmt.Println("|    backpack, inventory, i:                                   |")
	fmt.Println("|        these commands will show you your inventory           |")
	fmt.Println("|    stats, skills, s:                                         |")
	fmt.Println("|        _Not yet implemented_ will show skills and stats      |")
	fmt.Println("|                                                              |")
	fmt.Println("|                                                              |")
	fmt.Println("|  This game is still in development, more content will be     |")
	fmt.Println("|  added over time.                                            |")
	fmt.Println("|                                                              |")
	fmt.Println("|  Share any bugs and ideas with                               |")
	fmt.Println("|                  whiteRabbit @ HackFlag.org                  |")
	fmt.Println("|                                                              |")
	fmt.Println("|________________________________________________GoGoGame_v"+ version +"_|")
}